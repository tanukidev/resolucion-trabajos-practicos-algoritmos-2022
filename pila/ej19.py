'''Dada una pila de películas de las que se conoce su título, 
estudio cinematográfico y año de estreno, 
desarrollar las funciones necesarias para resolver las siguientes actividades:
a. mostrar los nombre películas estrenadas en el año 2014;[85]
b. indicar cuántas películas se estrenaron en el año 2018;
c. mostrar las películas de Marvel Studios estrenadas en el año 2016.'''
from pila import Pila

class Peliculas():
    def __init__(self, titulo, estudio, ano):
        self.titulo = titulo
        self.estudio = estudio
        self.ano = ano
    def __str__(self) -> str:
        return f'{self.titulo} {self.estudio} {self.ano}'
    titulo, estudio, ano = None, None, None
pila_pelis = Pila()
pila_pelis.apilar(Peliculas('godzila','sony', 2014))
pila_pelis.apilar(Peliculas('mi pobre angelito 4', 'universal', 2015))
pila_pelis.apilar(Peliculas('toy story 6', 'Pixar', 2016))
pila_pelis.apilar(Peliculas('infinity war 4: el regreso de Thanos', 'Marvel Studios', 2016))
pila_pelis.apilar(Peliculas('iron man 2: la venganza de electro', 'Marvel Studios', 2016))
pila_pelis.apilar(Peliculas('La era del hielo 7', 'Dreamworks', 2017))
pila_pelis.apilar(Peliculas('Rápido y furioso 20', 'universal', 2018))

def mostrar_por_ano (pila, ano):
    pila_aux = Pila()
    pila_ano = Pila()
    for i in range(pila.tamanio()):
        peli = pila.desapilar()
        if peli.ano == ano :
            pila_ano.apilar(peli)
        pila_aux.apilar(peli)
    return pila_aux, pila_ano
def mostrar_marvel(pila, estudio):
    pila_aux = Pila()
    pila_ano = Pila()
    
    
    for i in range(pila.tamanio()):
        peli = pila.desapilar()
        if peli.estudio == estudio :
            pila_ano.apilar(peli)
        pila_aux.apilar(peli)
    return pila_aux, pila_ano

def barrido(pila):
    for i in range(pila.tamanio()):
        print(pila.desapilar())

pila_pelis, ano2014 = mostrar_por_ano(pila_pelis, 2014)
pila_pelis, ano2018 = mostrar_por_ano(pila_pelis, 2018)
pila_pelis, marvel = mostrar_marvel(pila_pelis, 'Marvel Studios')
print('pelis del año 2014')
barrido(ano2014)
print('año 2018')
barrido(ano2018)
print('peliculas de marvel')
barrido(marvel)








