'''Dada una pila de personajes de Marvel Cinematic Universe (MCU), de los cuales se dispone de
su nombre y la cantidad de películas de la saga en la que participó, implementar las funciones
necesarias para resolver las siguientes actividades:
a. determinar en qué posición se encuentran Rocket Raccoon y Groot, tomando como posición uno la cima de la pila;[86]
b. determinar los personajes que participaron en más de 5 películas de la saga, además indicar la cantidad de películas en la que aparece;
c. determinar en cuantas películas participo la Viuda Negra (Black Widow);
d. mostrar todos los personajes cuyos nombre empiezan con C, D y G.'''
from pila import Pila
class Personajes():
    nombre= None
    peliculas=None



personaje = Personajes()
pila_personajes = Pila()

personaje.nombre = 'Rocket Raccoon y Groot'
personaje.peliculas = ['Guardianes de la galaxia', 'Guardianes de la galaxia 2', 'Infinity war', 'End Game']
pila_personajes.apilar(personaje)
personaje.nombre = 'BLack Widow'
personaje.peliculas = ['BLack widow 1', 'Black Widow 2']
pila_personajes.apilar(personaje)
personaje.nombre = 'Tony stark'
personaje.peliculas = ['Iron man', 'Iron man 2', 'Iron man 3', 'capitan america guerra civil', 'Spiderman homecoming', 'infinity war', 'end game']
pila_personajes.apilar(personaje)
personaje.nombre = 'Ant man'
personaje.peliculas = ['Ant man', 'Ant man 2']
pila_personajes.apilar(personaje)

def reapilar(pila):
    pila2 = Pila()
    for i in range(pila.tamanio):
        pila2.apilar(pila.desapilar)
    for i in range(pila2.tamanio):
        pila.apilar(pila2.desapilar)
    return pila

def posicion_pila(pila, buscado):
    pila2=reapilar(pila)
    i=1
    while (pila.tamanio !=0) and (pila.cima != buscado):
        pila.desapilar
        i+=1
    return pila2, i

def cant_pelis(pila):
    pila2=reapilar(pila)
    
    masde5 = []
    while (pila.tamanio !=0):
        personaje = pila.desapilar()
        if len(personaje.peliculas) >= 5:
            print(personaje)
        

    return pila2

