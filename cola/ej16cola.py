'''tilice cola de prioridad, para atender la cola de 
impresión tomando en cuenta el siguiente
criterio (1- empleados, 
2- staff de tecnologías de la información “TI”, 
3- gerente), y resuelva la
siguiente situación:
a.
cargue tres documentos de empleados (cada documento se representa solamente con
un nombre).
b. imprima el primer documento de la cola (solamente mostrar el nombre
 de este por pantalla).
c. cargue dos documentos del staff de TI.
d. cargue un documento del gerente.
e. imprima los dos primeros documentos de la cola.
f. cargue dos documentos de empleados y uno de gerente.
g. imprima todos los documentos de la cola de impresió'''



from cola import Cola

c = Cola()
# c.arribo(1)
# c.arribo(2)
# c.arribo(3)
# c.arribo(4)
# c.arribo(5)
# c.arribo(6)
# c.arribo(7)

def vaciar(c):
    while c.cola_vacia() != True:
        print(c.atencion())

def imprimir(c, cantidad):
    for i in range(cantidad):
        print(c.atencion())

def cargar(c, documento):
    c.arribo(documento)
def reapilar(c, ccopy):
    
    while c.cola_vacia() != True:
        ccopy.arribo(c.atencion())
    return ccopy


# vaciar(c)
# reapilar(c,ccopy)
# vaciar(ccopy)
#a
print('a')
cargar(c, 'empleado1')
cargar(c,'empleado2')
cargar(c,'empleado3')
#b
imprimir(c,1)
#c
cargar(c,'TI 1')
cargar(c,'TI 2')
#d
cargar(c, 'gerente')
#e
imprimir(c,2)
#f
cargar(c,'empleado 4')
cargar(c, 'gerente 2')
#g
vaciar(c)
