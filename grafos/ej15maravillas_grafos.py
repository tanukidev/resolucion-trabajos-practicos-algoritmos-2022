
from dataclasses import dataclass
from math import sqrt
from grafo_copy import Grafo
g = Grafo(False)

def distancia(a1,b1, a2,b2):
    aux = ((a2-a1) + (b2-b1))**2
    # print( sqrt(aux))
    # print(type(aux))
    return round(sqrt(aux))
s = ''
def anterior(lista):
    if len(lista) - 2 < 0:
        return lista[0]
    else:
        return lista[len(lista) - 2]
def ultimo(lista):
    return lista[len(lista)-1]

def isNatural(nombre):
    if nombre.endswith('natural') == True:
        return True
    else:
        return False
@dataclass
class Maravilla:
    name: str
    countries: str
    natural: bool
    lat: int
    long: int

#file = open('/home/tanuki/Documentos/uader/2do/algoritmos y estrucutras de datos/phyton/clases/maravillas.csv')
# file = open ('/home/tanuko/MEGA/uader/2do/algoritmos y estrucutras de datos/phyton/clases/maravillas.csv')
# a = []
# print(len(a))
# lineas =file.readlines()
# # print(type(lineas))
# # print(lineas)
# lineas.pop(0)  # quitar cabecera
# maravillasNaturales = []
# maravillasArtificiales= []
# maravilla = Maravilla(None, None, None, None, None)
i = 0
g.insertar_vertice('puerto princesa')
g.insertar_vertice('montaña de la mesa')
g.insertar_vertice('cataratas del iguazu')
g.insertar_arista('puerto princesa', 'montaña de la mesa', 300)
g.insertar_arista('montaña de la mesa', 'cataratas del iguazu', 20)
g.insertar_arista('cataratas del iguazu', 'puerto princesa', distancia(200,500, 100, 300))

# for linea in lineas:
#     datos = linea.split(';')    #separar en lineas
#     datos.pop(-1)               #eliminar el /n a lo último
#     # print(datos)
#     maravilla.name = datos[0]
#     maravilla.countries = datos[1]
#     maravilla.natural = isNatural(datos[2])
#     maravilla.lat = int(datos[3])
#     maravilla.long = int(datos[4])
#     if maravilla.natural == True:
#         maravillasNaturales.append(maravilla)
#     else:
#         maravillasArtificiales.append(maravilla)
#     g.insertar_vertice(maravilla.name)
'''Parque Nacional del río subterráneo de Puerto Princesa;Filipinas;natural;100;300;
Montaña de la Mesa;sudafrica;natural;1000;100;
Cataratas del Iguazú;Argentina;natural;200;500;'''


# for i in range(len(maravillasNaturales)):
#     g.insertar_arista(maravillasNaturales[i-1].name, maravillasNaturales[i].name,distancia(maravillasNaturales[i-1].lat, maravillasNaturales[i-1].long, maravillasNaturales[i].lat, maravillasNaturales[i].long))
    
#     g.insertar_arista(anterior(maravillasNaturales).name, ultimo(maravillasNaturales).name,distancia(anterior(maravillasNaturales).lat, anterior(maravillasNaturales).long, ultimo(maravillasNaturales).lat, ultimo(maravillasNaturales).long))

   
    # g.insertar_arista(maravillasArtificiales[i-1].name, maravillasArtificiales[i].name,distancia(maravillasArtificiales[i-1].lat, maravillasArtificiales[i-1].long, maravillasArtificiales[i].lat, maravillasArtificiales[i].long))


   

    # print(maravilla)
    # print(maravilla.name)
    # if maravillas[i].natural == True:
    #     g.insertar_vertice(maravilla.name)
    #     if maravillas[i-1].natural == True:
    #         g.insertar_arista(maravillas[i-1].name, maravillas[i].name,distancia(maravillas[i-1].lat, maravillas[i-1].long, maravillas[i].lat, maravillas[i].long))
    #     else:
    #         g.insertar_arista(maravillas[i+1].name, maravillas[i].name,distancia(maravillas[i-1].lat, maravillas[i-1].long, maravillas[i].lat, maravillas[i].long))

    # else:
    #     g.insertar_vertice(maravilla.name)
    #     if maravillas[i-1].natural == False:
    #         g.insertar_arista(maravillas[i-1].name, maravillas[i].name,distancia(maravillas[i-1].lat, maravillas[i-1].long, maravillas[i].lat, maravillas[i].long))
    #     else:
    #         g.insertar_arista(maravillas[i+1].name, maravillas[i].name,distancia(maravillas[i-1].lat, maravillas[i-1].long, maravillas[i].lat, maravillas[i].long))

     
g.marcar_no_visitado()
g.barrido_amplitud('puerto princesa')   
# g.barrido_vertice()
g.kruskal()

