'''Implementar los algoritmos necesarios para resolver las siguientes tareas sobre el grafo de la figura:
a. barrido en profundidad y amplitud partiendo de A, C, F;
b. el camino más corto de A hasta F, de C hasta D, de B hasta G;
c. agrega una arista de C hasta A, de C hasta B, de G hasta D y vuelva a ejecutar los puntos del
ítem anterior sin camino;'''

from grafo import Grafo

def existe_paso(g, origen, destino):
    print(' exsiste paso entre', origen, 'y', destino)
    g.existe_paso(origen, destino)
    g.marcar_no_visitado()
grafo = Grafo(dirigido=True)

grafo.insertar_vertice('A')
grafo.insertar_vertice('B')
grafo.insertar_vertice('C')
grafo.insertar_vertice('D')
grafo.insertar_vertice('E')
grafo.insertar_vertice('F')
grafo.insertar_vertice('G')

grafo.insertar_arista('A', 'D', 13)
grafo.insertar_arista('A', 'B', 15)
grafo.insertar_arista('B', 'C', 2)
grafo.insertar_arista('B', 'F', 12)
grafo.insertar_arista('C', 'E', 5)
grafo.insertar_arista('C', 'G', 27)
grafo.insertar_arista('C', 'A', 19)
grafo.insertar_arista('D', 'F', 39)
grafo.insertar_arista('D', 'G', 45)
grafo.insertar_arista('E', 'F', 1)
grafo.insertar_arista('F', 'G', 3)

#a.
print('barrido profundidad A')
grafo.barrido_profundidad('A')
grafo.marcar_no_visitado()
print('barrido profundidad C')
grafo.barrido_profundidad('C')
grafo.marcar_no_visitado()
print('barrido profundidad F')
grafo.barrido_profundidad('F')
grafo.marcar_no_visitado()
#b.
existe_paso(grafo, 'A', 'F')
existe_paso(grafo, 'C', 'D')
existe_paso(grafo, 'B', 'G')
print('c y d')
grafo.existe_paso('C', 'D')

#c.
print('punto c')
grafo.insertar_arista('C', 'A', 19)
grafo.insertar_arista('C', 'B', 2)
grafo.insertar_arista('G', 'D', 45)

existe_paso(grafo, 'A', 'F')
existe_paso(grafo, 'C', 'D')
existe_paso(grafo, 'B', 'G')