'''Dado un árbol con los nombre de los superhéroes y villanos de la saga Marvel Cinematic Univer-
se (MCU), desarrollar un algoritmo que contemple lo siguiente:
a. además del nombre del superhéroe, en cada nodo del árbol se almacenará un campo boo-
leano que indica si es un héroe o un villano, True y False respectivamente;
b. listar los villanos ordenados alfabéticamente;
c. mostrar todos los superhéroes que empiezan con C;
d. determinar cuántos superhéroes hay el árbol;
e. Doctor Strange en realidad está mal cargado. Utilice una búsqueda por proximidad para
encontrarlo en el árbol y modificar su nombre;
f. listar los superhéroes ordenados de manera descendente;
g. generar un bosque a partir de este árbol, un árbol debe contener a los superhéroes y otro a
los villanos, luego resolver las siguiente tareas:
//tomar un barrido reemplazar el print por una condición, si es villano insertar en un arbol si es héroe devolver en otro arbolg
I.determinar cuántos nodos tiene cada árbol;
II. realizar un barrido ordenado alfabéticamente de cada árbol
6.

'''
#from . algoritmos_2022 import arbol
from cola import Cola
from arbol import (
    arbolAcola,
    arbolAlista,
    
    nodoArbol,
    insertar_nodo,
    inorden_villano,
    inorden_empieza_con,
    contar_heroes,
    eliminar_nodo,
    inorden,
    por_nivel,
    postorden_heroes,
    crear_bosque,
    arbol_vacio,
    busqueda,
    contar_nodos
)
arbol = nodoArbol()
lambda nombre: nombre['info']
#g.
def crear_subarboles(lista, arbolVillano, arbolHeroe):
    for elem in lista:
        if elem['datos'] == True:
            insertar_nodo(arbolVillano, elem['info'], elem['datos'])
        else:
            insertar_nodo(arbolHeroe, elem['info'], elem['datos'])



def esVillano(voh):
    if voh == 'v':
        return True
    else:
        return False
def reemplazar(arbol, lista):
    lista
def listarVillano(lista):
    for personaje in lista:
        if personaje['datos'] == True:
            print(personaje['info'])
def listarHeroeConC(lista):
    for personaje in lista:
        if (personaje['info'].startswith("c")) or (personaje['info'].startswith("C")):
            print(personaje['info'])
def contar(lista, heroeOvillano):
    '''True si villano o False si heroe'''
    contadorH = 0
    contadorV = 0
    if heroeOvillano == True:
        for personaje in lista:
            if personaje['datos'] == True:
                contadorV+=1
        return contadorV
    else:
        for personaje in lista:
            if personaje['datos'] == False:
                contadorH+=1
        return contadorH


#cadena.startswith

listaVillano = []
lista = []
#     ['iron man', False],
#     ['thor', False],
#     ['red skull', True],
    

colarbol = Cola()
#file = open('/home/tanuki/Documentos/uader/2do/algoritmos y estrucutras de datos/phyton/resolucion-trabajos-practicos-algoritmos-2022/arboles/MCU.csv', mode='r')
file = open('/home/tanuko/MEGA/uader/2do/algoritmos y estrucutras de datos/phyton/resolucion-trabajos-practicos-algoritmos-2022/arboles/MCU.csv', mode='r')

lineas = file.readlines()
lineas.pop(0)  # quitar cabecera

#print(lineas)
for linea in lineas:
    dato = linea.split(';')
#    print(dato)
    insertar_nodo(arbol,dato[1],esVillano(dato[0]))
#inorden_villano(arbol)
#colarbol.arribo(arbol)
# arbolAcola(arbol, colarbol)
# while not colarbol.cola_vacia():
#     print(colarbol.atencion())

arbolAlista(arbol, lista)
#b.
print('villanos ->')
listarVillano(lista)
print('heroes ->')
#c.
listarHeroeConC(lista)
H=False
#d.
print('la cantidad de héroes', contar(lista, H))
#e.
encontrados = []
inorden_empieza_con(arbol, 'Dotor', encontrados,)
eliminar_nodo(arbol, encontrados[0]['info'])
encontrados[0]['info']='doctor Strange'
insertar_nodo(arbol, encontrados[0]['info'], encontrados[0]['datos'] )
print('--')
inorden(arbol)
lista = []
arbolAlista(arbol, lista)
#g.

arbolHeroe = nodoArbol()
arbolVillano = nodoArbol()
crear_subarboles(lista, arbolVillano, arbolHeroe)
print('heroes--')
inorden(arbolHeroe)

print('--villanos')
inorden(arbolVillano)
#I.
contadorVillano = [0]
contadorHeroe = [0]

contar_nodos(arbolVillano, contadorVillano)
contar_nodos(arbolHeroe, contadorHeroe)

# heroes = []
# villanos = []
# arbolAlista(arbolHeroe, heroes)
# arbolAlista(arbolVillano, villanos)

# print(f' los villanos {len(heroes)} y los heroes {len(villanos)}')
print('los heroes son ', contadorHeroe,'y los villanos', contadorVillano)


print('--')
