'''El problema de la mochila Jedi. Suponga que un Jedi (Luke Skywalker, Obi-Wan Kenobi, Rey u
otro, el que más le guste) está atrapado, pero muy cerca está su mochila que contiene muchos
objetos. Implementar una función recursiva llamada “usar la fuerza” que le permita al Jedi “con
ayuda de la fuerza” realizar las siguientes actividades:
a. sacar los objetos de la mochila de a uno a la vez hasta encontrar un sable de luz o que no
queden más objetos en la mochila;
b. determinar si la mochila contiene un sable de luz y cuantos objetos fueron necesarios sacar para encontrarlo;
c. Utilizar un vector para representar la mochila'''




mochila = ['casco','zapato','traje','sable']
def sacar_objeto (mochila, i):
    
    if mochila[i] == 'sable':
        print('la cantidad de objetos que se sacaron antes de encontrar el sable fueron ', i)
        return None
    else:
        if i < len(mochila):
            i=i+1
            sacar_objeto(mochila, i)
        else:

            print('no hay sable de luz')
print(mochila[1])
i=0
sacar_objeto(mochila,i)



