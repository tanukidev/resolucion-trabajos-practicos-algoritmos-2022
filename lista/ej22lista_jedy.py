'''Se dispone de una lista de todos los Jedi, 
de cada uno de estos se conoce su nombre, maestros,
colores de sable de luz usados y especie. 
implementar las funciones necesarias para resolver las
actividades enumeradas a continuación:
a. listado ordenado por nombre y por especie;
b. mostrar toda la información de Ahsoka Tano y Kit Fisto;
c. mostrar todos los padawan de Yoda y Luke Skywalker, es decir sus aprendices;
d. mostrar los Jedi de especie humana y twi'lek;
e. listar todos los Jedi que comienzan con A;
f. mostrar los Jedi que usaron sable de luz de más de un color;
g. indicar los Jedi que utilizaron sable de luz amarillo o violeta;
h. indicar los nombre de los padawans de Qui-Gon Jin y Mace Windu, si los tuvieron.'''

#el archivo es jedi.text
# 
# .
# from algoritmos_2022.lista import Lista
# file = open('jedis', mode ='r', encoding='UTF-8')
# lineas = file.readlines()


# print(file.readlines())





class Jedi(self):

    self.nombre = None
    self.especie = None
    self.maestro = None
    self.sable_luz = None

    def __init__(self, nombre, especie, maestro, sable_luz):
        self.nombre = None
        self.especie = None
        self.maestro = maestro
        self.sable_luz = sable_luz

#     def __str__(self):
#         return f"{self.nombre} | {self.especie} | {self.maestro} | {self.sable_luz}"
# #salto de linea |

# lista_jedi = Lista()
# lista_jedi2 = Lista()

# file = open('jedis.txt')
# lineas = file.readlines()

# lista = []

# lineas.pop(0)  # quitar cabecera
# for linea in lineas:
#     datos = linea.split(';')
#     datos.pop(-1)
#     # print(datos[4].split('/'))
#     lista_jedi.insertar(Jedi(datos[0],
#                              datos[2],
#                              datos[3].split('/'),
#                              datos[4].split('/')),
#                         campo='nombre')
#     lista_jedi2.insertar(Jedi(datos[0],
#                               datos[2],
#                               datos[3],
#                               datos[4].split('/')),
#                          campo='especie')
#     lista.append(Jedi(datos[0],
#                       datos[2],
#                       datos[3].split('/'),
#                       datos[4].split('/')))
# # !
# lista_jedi.barrido()
# print()
# # lista_jedi2.barrido()

# dato = lista_jedi.busqueda('kit fisto', 'nombre')
# if dato:
#     print(f'el Jedi {dato.info}')
# else:
#     print('el Jedi no esta en la lista')

# print()
# lista_jedi.barrido_jedi_master()

# print()
# lista_jedi.barrido_comienza_con(['a'])



class Jedi:

    def __init__(self, nombre, especie, maestro, sable_luz):
        self.nombre = nombre
        self.especie = especie
        self.maestro = maestro
        self.sable_luz = sable_luz

    def __str__(self):
        return f"{self.nombre} | {self.especie} | {self.maestro} | {self.sable_luz}"


lista_jedi = Lista()
lista_jedi2 = Lista()

file = open('jedis.txt')
lineas = file.readlines()

lista = []
#pop quita un elemento de la lista según el índice
#luego de hacer el split se guarda en datos, cada columna un elemento
lineas.pop(0)  # quitar cabecera
for linea in lineas:
    datos = linea.split(';')    #separar en lineas
    datos.pop(-1)               #eliminar el /n a lo último
    # print(datos[4].split('/'))
    lista_jedi.insertar(Jedi(datos[0],
                             datos[2],
                             datos[3].split('/'),
                             datos[4].split('/')),
                        campo='nombre')
    lista_jedi2.insertar(Jedi(datos[0],
                              datos[2],
                              datos[3],
                              datos[4].split('/')),
                         campo='especie')
    # lista.append(Jedi(datos[0],
    #                   datos[2],
    #                   datos[3].split('/'),
    #                   datos[4].split('/')))
# !
lista_jedi.barrido()
print()
# lista_jedi2.barrido()

dato = lista_jedi.busqueda('kit fisto', 'nombre')
if dato:
    print(f'el Jedi {dato.info}')
else:
    print('el Jedi no esta en la lista')

print()
lista_jedi.barrido_jedi_master()

print()
lista_jedi.barrido_comienza_con(['a'])
